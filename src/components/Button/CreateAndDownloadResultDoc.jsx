import { Button } from "antd";
import {
  Document,
  Paragraph,
  Packer,
  Footer,
  AlignmentType,
  TextRun,
  ExternalHyperlink,
} from "docx";
import { FileWordFilled } from "@ant-design/icons";
import { formatDateToYYYYMMDDHHMM, getCookie } from "../../utils";
import { useState } from "react";
import { messageToolStructureNotSupport } from "../../utils/history";

export default function CreateAndDownloadResultDoc({
  name,
  content,
  title,
  customFileName = null,
  customUsernameCreated = null,
  createdAt = null,
}) {
  const [nameUser] = useState(getCookie("name"));
  const [username] = useState(
    customUsernameCreated ? customUsernameCreated : getCookie("username")
  );

  const handleCreateAndDownloadDocument = async ({
    content = "",
    title = "",
  }) => {
    const doc = new Document({
      sections: [
        {
          properties: {},
          children: [
            new Paragraph({
              children: [
                new TextRun({
                  text: title,
                  bold: true,
                  size: 32,
                  font: "Poppins",
                  color: "#1CAFC2",
                }),
              ],
            }),
            new Paragraph({
              children: [
                new TextRun({
                  text: `By: ${nameUser}/${username}`,
                  size: 18,
                  font: "Poppins",
                }),
              ],
              alignment: AlignmentType.RIGHT,
            }),
            new Paragraph({
              children: [
                new TextRun({
                  text: `Created at ${
                    createdAt ? new Date(createdAt) : new Date().toString()
                  }`,
                  size: 16,
                  font: "Poppins",
                }),
              ],
              alignment: AlignmentType.RIGHT,
            }),
            ...content.split("\n").map(
              (item) =>
                new Paragraph({
                  children: [
                    new TextRun({
                      text: item,
                      size: 24,
                      font: "Poppins",
                    }),
                  ],
                })
            ),
          ],
          footers: {
            default: new Footer({
              children: [
                new Paragraph({
                  children: [
                    new TextRun({
                      text: "Powered by Reelsights - ",
                      size: 13,
                      font: "Poppins",
                    }),
                    new ExternalHyperlink({
                      children: [
                        new TextRun({
                          text: "https://reelsightsai.com",
                          size: 13,
                          font: "Poppins",
                          underline: true,
                        }),
                      ],
                      link: "https://reelsightsai.com",
                    }),
                  ],
                  alignment: AlignmentType.RIGHT,
                }),
              ],
            }),
          },
        },
      ],
    });
    await Packer.toBlob(doc).then((blob) => {
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement("a");
      a.href = url;
      const fileName = customFileName
        ? customFileName
        : name + "-" + formatDateToYYYYMMDDHHMM(new Date());
      a.download = fileName;
      a.click();
    });
  };

  if (content === messageToolStructureNotSupport) return null;

  return (
    <Button
      onClick={() => handleCreateAndDownloadDocument({ content, title })}
      className="bg-[#08589E] text-white flex items-center justify-center"
      icon={<FileWordFilled />}
    >
      <span className="hidden md:block">Download Document</span>
    </Button>
  );
}
