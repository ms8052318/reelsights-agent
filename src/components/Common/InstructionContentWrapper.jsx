import React from "react";
import { Carousel } from "antd";

export default function InstructionContentWrapper({ children }) {
  return (
    <Carousel autoplay pauseOnHover>
      {children}
    </Carousel>
  );
}
