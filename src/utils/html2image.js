import html2canvas from 'html2canvas';

export const exportAsImageAndDownload = async (el, fileName) => {
  console.log(el);
  const canvas = await html2canvas(el);
  //   export as image
  const imgData = canvas.toDataURL('image/png');
  //  download
  const link = document.createElement('a');
  link.download = `${fileName}.png`;
  link.href = imgData;
  link.click();
};
