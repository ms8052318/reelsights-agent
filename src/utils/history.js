export const messageToolStructureNotSupport =
  'The structure of the data is not suitable for displaying in text format. Please use the "View" button to view in the tool.';
