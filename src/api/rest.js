import axios from "axios";
import qs from "querystringify";
import { message } from "antd";
import {
  deleteCookie,
  getCookie,
  sendMessageExpired,
  sendMessageNotEnough,
} from "../utils";

const baseUrl = process.env.REACT_APP_RS_ENDPOINT;

const TIMEOUT = 300000;

axios.defaults.withCredentials = true;

const clearAuthCookies = () => {
  deleteCookie("token");
  deleteCookie("refresh_token");
  deleteCookie("emailGG");
  if (getCookie("loginType") === "google") {
    deleteCookie("loginType");
    deleteCookie("username");
    deleteCookie("name");
  }
};

const handleResponse = async (res) => {
  if (!res) {
    clearAuthCookies();
    window.location.href = "/sign-in";
    return;
  }
  if (res?.status === 401) {
    clearAuthCookies();
    window.location.href = "/sign-in";
    return;
  } else if (res?.status === 418) {
    // sendMessageCredit();
    return;
  } else if (res?.status === 423) {
    sendMessageNotEnough();
    return res;
  } else if (res?.status === 426) {
    sendMessageExpired();
    return res;
  } else if (res?.status === 400) {
    return;
  } else {
    if (window.location.pathname === "/analysis/youtube/video-formula-search") {
      message.info(
        "Your Google token has expired. Please log out and log in again to continue using it."
      );
    }
    return res;
  }
};

const redirectToSomethingWentWrongScreen = () => {
  return setTimeout(() => {
    // window.location.href = '/500';
  }, TIMEOUT);
};

const generateBaseUrl = (api) => {
  if (api === "RS") {
    return baseUrl;
  }
};

export const rest = {
  getMapping: async (api, endpoint, params, timeout = TIMEOUT, isAuth) => {
    let options = {};
    options = {
      headers: {
        username: "hoquanglam090@gmail.com",
      },
    };

    const handleTimeout = redirectToSomethingWentWrongScreen(timeout);
    try {
      const res = await axios.get(
        `${generateBaseUrl(api) + endpoint}${
          JSON.stringify(params) !== "{}" ? "?" : ""
        }${qs.stringify({
          ...params,
        })}`,
        options.headers ? options : {}
      );
      clearTimeout(handleTimeout);
      return res.data;
    } catch (err) {
      clearTimeout(handleTimeout);
      return await handleResponse(
        err?.response,
        `${generateBaseUrl(api) + endpoint}${
          JSON.stringify(params) !== "{}" ? "?" : ""
        }${qs.stringify({
          ...params,
        })}`
      );
    }
  },
  postMapping: async (
    api,
    endpoint,
    params,
    timeout = TIMEOUT,
    overrideHeader = {}
  ) => {
    let options = {};
    options = {
      headers: {
        username: "hoquanglam090@gmail.com",
        ...overrideHeader,
      },
    };

    const handleTimeout = redirectToSomethingWentWrongScreen(timeout);
    try {
      const res = await axios.post(
        generateBaseUrl(api) + endpoint,
        { ...params },
        options
      );
      clearTimeout(handleTimeout);
      return res.data;
    } catch (_) {
      try {
        const res = await axios.post(
          generateBaseUrl(api) + endpoint,
          { ...params },
          options
        );
        clearTimeout(handleTimeout);

        return res.data;
      } catch (err) {
        clearTimeout(handleTimeout);
        return await handleResponse(err?.response, {
          baseUrl: generateBaseUrl(api) + endpoint,
          params,
        });
      }
    }
  },
};
