import { Link, useLocation } from "react-router-dom";
import { dataRoutesReelSy, dataRoutesReelsightsAI } from "../../data";

export default function ReelsyContentCreationNav() {
  const currentPath = useLocation().pathname;

  return (
    <nav className="hide-scrollbar flex w-full items-center overflow-auto bg-[#d7eeed] pt-[50px] pb-[35px] pl-[100px] font-semibold">
      <div className="pr-3 text-[18px] font-bold text-[#141f40]">
        AI Agent - Content Creation
      </div>

      <Link
        id="tour-step-3"
        to={dataRoutesReelSy[0]}
        className={`whitespace-nowrap border-[#1e263d] py-1 px-2 ${
          currentPath === dataRoutesReelSy[0]
            ? "text-[#2E8A4F]"
            : "text-[#1e263d]"
        }`}
      >
        Mini Campaign
      </Link>
      <Link
        id="tour-step-5"
        to={dataRoutesReelSy[1]}
        className={`whitespace-nowrap border-l border-[#1e263d] py-1 px-2 ${
          currentPath.includes(dataRoutesReelSy[1])
            ? "text-[#2E8A4F]"
            : "text-[#141f40]"
        }`}
      >
        Strategy Analysis
      </Link>
    </nav>
  );
}
