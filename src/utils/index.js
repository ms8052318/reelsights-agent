import moment from "moment";
import { message, notification } from "antd";
import slugify from "slugify";

export const getAuthToken = () => {
  return {
    token: getCookie("token") || undefined,
    refresh_token: getCookie("refresh_token") || "",
    tt_access_token: getCookie("tt_access_token") || "",
    tt_refresh_token: getCookie("tt_refresh_token") || "",
    username: getCookie("username") || "",
    creditName: getCookie("creditName") || "",
    name: getCookie("name") || "",
    loginType: getCookie("loginType") || "",
  };
};

export const getCookie = (cookieName) => {
  let cookie = {};
  document.cookie.split(";").forEach(function (el) {
    let [key, value] = el.split("=");
    cookie[key.trim()] = value;
  });
  return cookie[cookieName];
};

export const setCookie = (
  name,
  value,
  config = {
    path: "",
    maxAge: null,
  }
) => {
  let configString = "";
  if (config.maxAge !== null) {
    configString += "; max-age=" + config.maxAge;
  }
  if (config.path) {
    configString += "; path=" + config.path;
  }
  document.cookie = name + "=" + (value || "") + configString;
};

export const deleteCookie = (name) => {
  document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
};

export const nFormatter = (num) => {
  if (num >= 1000000000000) {
    return (num / 1000000000000).toFixed(2).replace(/\.0$/, "") + "T";
  }
  if (num >= 1000000000) {
    return (num / 1000000000).toFixed(2).replace(/\.0$/, "") + "B";
  }
  if (num >= 1000000) {
    return (num / 1000000).toFixed(2).replace(/\.0$/, "") + "M";
  }
  if (num >= 1000) {
    return (num / 1000).toFixed(2).replace(/\.0$/, "") + "K";
  }
  return num;
};

export const convertCaseToTitle = (str) => {
  return str.replace(/([A-Z])/g, " $1").replace(/^./, function (str) {
    return str.toUpperCase();
  });
};

export const loggedIn = () => {
  const loginType = getCookie("loginType") || "";
  if (loginType !== "") return true;
  return false;
};

export const isLoggedInGoogle = () => {
  const loginType = getCookie("loginType") || "";
  const emailGG = getCookie("emailGG") || "";
  const { token, refresh_token } = getAuthToken();
  console.log("isLoggedInGoogle -> token", loginType);
  console.log("isLoggedInGoogle -> token", token);

  if (loginType === "google" && token && refresh_token) return true;
  // Login account nhưng mà có token và emailGG thì cũng là login google nhưng trước đó có login account
  if (loginType === "account" && token) return true;
  return false;
};

export const isLoggedInTikTok = () => {
  const loginType = getCookie("loginType") || "";
  const { tt_refresh_token, tt_access_token } = getAuthToken();
  if (
    loginType === "tiktok" &&
    tt_refresh_token !== "" &&
    tt_access_token !== ""
  )
    return true;
  return false;
};

export const isLoggedInLinkedin = () => {
  const li_Tk = getCookie("li_Tk") || "";
  if (li_Tk !== "") return true;
  return false;
};

export const isLoggedInTwitter = () => {
  const tw_Tk = getCookie("tw_Tk") || "";
  if (tw_Tk !== "") return true;
  return false;
};

export const convertFromISO8601ToHHMMSS = (iso8601TimeString = "") => {
  const d = moment.duration(iso8601TimeString);
  return moment.utc(d.asSeconds() * 1000).format("HH:mm:ss");
};

export function formatSecond(second) {
  return new Date(second * 1000).toISOString().slice(11, 23).replace(".", ",");
}

export const getSrtText = (fullRes) => {
  const allScripts = fullRes?.map((item) => item.text);
  const allTime = fullRes?.map(
    (item) =>
      `${formatSecond(item.start)} --> ${formatSecond(
        item.start + item.duration
      )}`
  );
  let scriptText = "";
  for (let i = 0; i < allScripts.length; i++) {
    scriptText += i + 1;
    scriptText += "\n";
    scriptText += allTime[i];
    scriptText += "\n";
    scriptText += allScripts[i];
    scriptText += "\n\n";
  }
  return scriptText;
};

export const getSrtTextWithoutTime = (fullRes = []) => {
  if (!Array.isArray(fullRes)) return "";
  let srtTextWithoutTime = fullRes?.map((item) => item.text).join("\n");
  return srtTextWithoutTime;
};

export const handleCopyAllScripts = (fullRes) => {
  const scriptText = getSrtText(fullRes);
  navigator.clipboard.writeText(scriptText);
  message.success("Copied to clipboard!");
};

export const handelCopyAllScriptsWithoutTime = (fullRes) => {
  const scriptText = getSrtTextWithoutTime(fullRes);
  navigator.clipboard.writeText(scriptText);
  message.success("Copied to clipboard!");
};

export const downloadSrtFile = (fullRes, language, videoName) => {
  const element = document.createElement("a");
  const file = new Blob([getSrtText(fullRes)], { type: "text/plain" });
  element.href = URL.createObjectURL(file);
  element.download = `[${language}]-${videoName ? videoName : "video"}.srt`;
  document.body.appendChild(element); // Required for this to work in FireFox
  element.click();
};

export const downloadSrtFileAsText = (fullRes, language, videoName) => {
  const element = document.createElement("a");
  const file = new Blob([getSrtTextWithoutTime(fullRes)], {
    type: "text/plain",
  });
  element.href = URL.createObjectURL(file);
  element.download = `[${language}]-${videoName ? videoName : "video"}.txt`;
  document.body.appendChild(element); // Required for this to work in FireFox
  element.click();
};

export const getShortenedParagraph = (fullRes = [], maxLength) => {
  const allScripts = fullRes?.map((item) => item.text);
  let scriptText = "";
  for (let i = 0; i < allScripts.length; i++) {
    if (scriptText.length + String(allScripts[i]).length > maxLength) {
      break;
    } else {
      scriptText += allScripts[i] + " ";
    }
  }
  if (scriptText.length == 0) {
    return allScripts[0];
  }
  return scriptText;
};

export const isValidatedTweetLink = (url) => {
  const regex =
    /^(((https?):\/\/)?(www.)?)twitter\.com(\/(\w){1,15})\/status\/[0-9]{19}$/;
  return regex.test(url);
};

export const isValidatedTwitterProfileLink = (url) => {
  const regex = /^(((https?):\/\/)?(www.)?)twitter\.com(\/(\w){1,15})$/;
  return regex.test(url);
};

export const isValidatedLinkedinProfileLink = (url) => {
  const regex = /^(((https?):\/\/)?(www.)?)linkedin.com(\/in\/(\w){1,100}).*$/;
  return regex.test(url);
};

export const sendMessageErrorCommon = () => {
  // message.error(
  //   'The engine is currently overloaded, please try again later. If the problem persists, please contact us at tech@reelsights.com. Cause: Our servers are experiencing high traffic. Solution: Please retry your requests after a brief wait.',
  // );
};

export const sendMessageCredit = () => {
  message.error("You don't have enough credit. Consider upgrading for more.");
};

export const sendMessageNotEnough = () => {
  message.error("Your Video doesn't have enough comment.");
};

export const sendMessageExpired = () => {
  message.error(
    "Your account has expired. Please renew your subscription to continue using our service."
  );
};

export const getJavascriptDate = (dateString, format) => {
  let date;

  if (format === "dd/mm/yyyy") {
    const [day, month, year] = dateString.split("/").map(Number);
    date = new Date(year, month - 1, day);
  }

  return date;
};

export const getFileBinary = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
    }
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

export const normalizeString = (str) =>
  str
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/đ/g, "d")
    .replace(/Đ/g, "D")
    .replaceAll("_", " ")
    .trim();

export const getBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
    } else {
      reject("File is not valid");
    }
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

export const formatDateToYYYYMMDDHHMM = (date) => {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Months are zero-based, so we add 1
  const day = String(date.getDate()).padStart(2, "0");
  const hours = String(date.getHours()).padStart(2, "0");
  const minutes = String(date.getMinutes()).padStart(2, "0");

  return `${year}${month}${day}${hours}${minutes}`;
};

export const convertHexToRgb = (hex) => {
  const hexCode = hex.replace("#", "");
  const r = parseInt(hexCode.substring(0, 2), 16);
  const g = parseInt(hexCode.substring(2, 4), 16);
  const b = parseInt(hexCode.substring(4, 6), 16);
  return [r, g, b];
};

export const scrollToToolResultContainer = () => {
  const toolResultContainer = document.getElementById("tool-result-container");
  if (toolResultContainer) {
    setTimeout(() => {
      toolResultContainer.scrollIntoView({ behavior: "smooth" });
    }, 1);
  }
};

export const getSlug = (title, id) => {
  return `${slugify(title, {
    replacement: "-", // replace spaces with replacement character, defaults to `-`
    remove: undefined, // remove characters that match regex, defaults to `undefined`
    lower: true, // convert to lower case, defaults to `false`
    strict: true, // strip special characters except replacement, defaults to `false`
  })}-${id}`;
};

export const createParamsTranslationAWS = ({ text, language }) => {
  return {
    text: text.replaceAll("\n", "<br/>"),
    output: language,
    email: getCookie("username") || "",
  };
};

export const getAllObjectValues = (obj, allowKeys = []) => {
  let values = [];

  // Function to recursively traverse the object
  function traverse(obj) {
    for (let key in obj) {
      if (allowKeys.includes(key) && typeof obj[key] !== "object") {
        values.push(obj[key]); // Push the value into the values array only if it's in allowKeys
      } else if (typeof obj[key] === "object" && obj[key] !== null) {
        traverse(obj[key]); // Recursive call for nested objects
      }
    }
  }

  traverse(obj); // Start traversing from the root object
  return values;
};

export const getRepairedDataChatGemini = (dataChat = []) =>
  dataChat.map((item) => {
    return {
      parts: [
        {
          text: item.content,
        },
      ],
      role: item.role === "assistant" ? "model" : "user",
    };
  });
