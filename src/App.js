import logo from "./logo.svg";
import "./App.css";
import "./styles/custom.css";
import "antd/dist/antd.min.css";

import ReelSyAIContent from "./pages/Research/ReelSyAiContent/ReelSyAiContent";
import { Switch, Route } from "react-router-dom";
import { dataRoutesReelSy } from "./data";
import ReelSyAiStrat from "./pages/Research/ReelSyAiStrat";

function App() {
  return (
    <Switch>
      <Route
        exact
        path={dataRoutesReelSy[0]}
        component={ReelSyAIContent}
      ></Route>
      <Route exact path={dataRoutesReelSy[1]} component={ReelSyAiStrat}></Route>
    </Switch>
  );
}

export default App;
