import React from "react";

export default function ToolLayout({ children }) {
  return (
    <div
      className="relative min-h-screen max-w-full bg-white 2xl:flex"
      id="tour-step-6"
    >
      <main className="flex-1">{children}</main>
    </div>
  );
}
