import { sendMessageErrorCommon } from '.';

const banWords = [
  'general knowledge',
  "While i don't have access",
  'as an ai language model',
  'as a language model ai',
  'january 2021',
  'february 2021',
  'march 2021',
  'april 2021',
  'may 2021',
  'june 2021',
  'july 2021',
  'august 2021',
  'september 2021',
  'october 2021',
  'november 2021',
  'december 2021',
  'as an ai model',
  'without access to specific',
  'without access to the specific',
  'hi reelsy',
  'real-time information',
  'real-time data',
  'ability to browse the internet',
  'general insights',
  'trustworthy sources',
  'verified or factual.',
];

export const getQueryScriptCreationYoutube = ({
  company,
  product,
  keywords,
  type_of_video,
  goal,
  tone_of_voice,
  num_keywords,
  music,
}) => {
  let query = `Act as a: Content Creator, create a youtube script\n`;
  query += `based on the following criterias:\n`;
  query += `${company ? `Company: ${company}\n` : ''}`;
  query += `${product ? `Product: ${product}\n` : ''}`;
  query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
  query += `${type_of_video ? `Type of video: ${type_of_video}\n` : ''}`;
  query += `${goal ? `Goal: ${goal}\n` : ''}`;
  query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
  query += `${num_keywords ? `Number of keywords: ${num_keywords}\n` : ''}`;
  query += `${music ? `Music: ${music}\n` : ''}`;
  return query;
};

export const getQueryContentCreationYoutube = ({
  company,
  goal,
  keywords,
  tone_of_voice,
  num_keywords,
  type_content,
}) => {
  let query = `Act as a: Content Creator, create a youtube ${type_content}\n`;
  query += `based on the following criterias:\n`;
  query += `${company ? `Company: ${company}\n` : ''}`;
  query += `${goal ? `Goal: ${goal}\n` : ''}`;
  query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
  query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
  query += `${num_keywords ? `Number of keywords: ${num_keywords}\n` : ''}`;
  return query;
};

export const getQueryCommonCreationYoutube = (
  { company, product, keywords, type_of_video, goal, tone_of_voice, num_keywords, music },
  typeOfQuery,
) => {
  let query = '';
  switch (typeOfQuery) {
    case 'script':
      query = `Act as a: Content Creator, create a youtube ${typeOfQuery}\n`;
      query += `based on the following criterias:\n`;
      query += `${company ? `Company: ${company}\n` : ''}`;
      query += `${product ? `Product: ${product}\n` : ''}`;
      query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
      query += `${type_of_video ? `Type of video: ${type_of_video}\n` : ''}`;
      query += `${goal ? `Goal: ${goal}\n` : ''}`;
      query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
      query += `${num_keywords ? `Number of keywords: ${num_keywords}\n` : ''}`;
      query += `${music ? `Music: ${music}\n` : ''}`;
      return query;
    case 'title':
    case 'description':
    case 'action':
      query = `Act as a: Content Creator, create a youtube ${typeOfQuery}\n`;
      query += `based on the following criterias:\n`;
      query += `${company ? `Company: ${company}\n` : ''}`;
      query += `${product ? `Product: ${product}\n` : ''}`;
      query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
      query += `${type_of_video ? `Type of video: ${type_of_video}\n` : ''}`;
      query += `${goal ? `Goal: ${goal}\n` : ''}`;
      query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
      query += `${music ? `Music: ${music}\n` : ''}`;
      return query;
    case 'hashtags':
      query = `Find ${typeOfQuery} for a youtube video \n`;
      query += `based on the following criterias:\n`;
      query += `${product ? `Product: ${product}\n` : ''}`;
      query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
      query += `${type_of_video ? `Type of video: ${type_of_video}\n` : ''}`;
      query += `${goal ? `Goal: ${goal}\n` : ''}`;
      query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
      query += `${music ? `Music: ${music}\n` : ''}`;
      return query;
    case 'music':
      query = `Find ${music} music for a youtube video \n`;
      query += `based on the following criterias:\n`;
      query += `${product ? `Product: ${product}\n` : ''}`;
      query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
      query += `${type_of_video ? `Type of video: ${type_of_video}\n` : ''}`;
      query += `${goal ? `Goal: ${goal}\n` : ''}`;
      query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
      return query;
    default:
      return '';
  }
};

export const getQueryTweetCreation = ({
  company,
  goal,
  keywords,
  language,
  product,
  tone_of_voice,
}) => {
  let query = 'Create 5 tweet for Twitter Account following the criteria below:';
  query += `${company ? `Company: ${company}\n` : ''}`;
  query += `${goal ? `Goal: ${goal}\n` : ''}`;
  query += `${keywords ? `Keywords: ${keywords}\n` : ''}`;
  query += `${language ? `Language: ${language}\n` : ''}`;
  query += `${product ? `Product: ${product}\n` : ''}`;
  query += `${tone_of_voice ? `Tone of voice: ${tone_of_voice}\n` : ''}`;
  return query;
};

export const handleAndGetDataCreation = (data) => {
  if (Number(data?.status) === 200 && data?.content) {
    let content = data?.content || '';
    // content = removeSentenceContainingText(content, banWords);
    return content;
  } else {
    throw new Error('Error when getting data in handleAndGetDataCreation, please try again');
  }
};

export const handleAndGetDataAWSTranslation = (data) => {
  let content = handleAndGetDataCreation(data);
  return content.replaceAll('<br/>', '\n');
};

export const removeSentenceContainingText = (paragraph = '', text = []) => {
  let modifiedParagraph = paragraph;

  // Define a regular expression for sentence splitting
  const sentenceSplitter = /[.。;!?]/;

  text.forEach((banWord) => {
    var sentences = modifiedParagraph.split(sentenceSplitter);
    sentences = sentences.filter(
      (sentence) => sentence.toLowerCase().trim().includes(banWord.toLowerCase()) === false,
    );
    modifiedParagraph = sentences.join('.').trim();
  });

  return modifiedParagraph;
};

export const removeLastSentence = (text) => {
  let sentences = text.split('.');
  sentences = sentences.filter((item) => item !== '');
  sentences.pop();
  return removeSentenceContainingText(sentences.join('.'), ['September 2021']);
};

export const handleContentReelsy = (text) => {
  let result = text;
  // remove last sentence
  result = removeSentenceContainingText(result, banWords);
  return result;
};

export const getTextBetweenMarkers = (input = '', marker = '') => {
  const startIndex = input.indexOf(marker);
  const endIndex = input.lastIndexOf(marker);

  if (startIndex === -1 || endIndex === -1 || startIndex === endIndex) {
    return null;
  }

  return input.substring(startIndex + marker.length, endIndex).trim();
};

export const removeHiReelsyFromSentence = (inputString) => {
  const removeText = 'Hi ReelSy,';
  return inputString.replace(removeText, '');
};
