import React from 'react';
import ReelsyContentCreationNav from '../components/Nav/ReelsyContentCreationNav';
import ToolLayouts from './ToolLayout';

export default function ReelsyContentCreationLayout({ children }) {
  return (
    <ToolLayouts>
      <ReelsyContentCreationNav />
      {children}
    </ToolLayouts>
  );
}
