import React from 'react';

export default function RSMarketingIcon({ className }) {
  return <img className={className} src={'/images/reelsy-ai-icon.webp'} alt="rs-marketing" />;
}
