import { useState } from 'react';
import { LayoutOutlined, MinusOutlined, PlusOutlined, DownOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

export default function ToolResultRSAIContainer({
  children,
  isShow = false,
  isLoading = false,
  isWrapped = false,
}) {
  const [showFull, setShowFull] = useState(true);
  if (!isShow) return null;
  if (isLoading)
    return (
      <div
        className={`relative mx-auto max-w-full bg-gray-100 py-16  px-10  `}
        id="tool-result-container">
        <div className="bg-linear-gradient-3 flex justify-between rounded-tl-[20px] px-4 py-2 text-[1.2rem] font-medium text-gray-200">
          <div className="flex items-center">
            <LayoutOutlined />
            <span className="ml-4">Results</span>
          </div>

          <div className="flex items-center justify-between">
            <div className="flex space-x-5 text-[0.8rem]">
              {showFull ? (
                <MinusOutlined onClick={() => setShowFull(false)} />
              ) : (
                <PlusOutlined onClick={() => setShowFull(true)} />
              )}
            </div>
          </div>
        </div>

        <div className="py-16 text-center">
          Getting Information... <Spin className="ml-2" />
        </div>
      </div>
    );

  return (
    <div className={`mx-auto  max-w-full bg-gray-100 px-10 py-16 `} id="tool-result-container">
      <div className="bg-linear-gradient-3 flex justify-between rounded-tl-[20px] px-4 py-2 text-[1.2rem] font-medium text-gray-200">
        <div className="flex items-center">
          <LayoutOutlined />
          <span className="ml-4">Your AI Agent’s Recommendation</span>
        </div>

        <div className="flex items-center justify-between">
          <div className="flex space-x-5 text-[0.8rem]">
            {showFull ? (
              <MinusOutlined onClick={() => setShowFull(false)} />
            ) : (
              <PlusOutlined onClick={() => setShowFull(true)} />
            )}
          </div>
        </div>
      </div>

      <div className="relative">
        <div className={`bg-white p-5 ${showFull ? '' : 'h-[50px] overflow-hidden'}`}>
          {children}
        </div>
        {showFull === false && (
          <button
            onClick={() => setShowFull(true)}
            className="absolute bottom-0 left-0 flex h-[50px] w-full items-center justify-center space-x-2 bg-gray-100 bg-black/30 text-white shadow-xl backdrop-blur-sm hover:opacity-50">
            <span>Show More</span>
            <DownOutlined />
          </button>
        )}
      </div>
    </div>
  );
}
