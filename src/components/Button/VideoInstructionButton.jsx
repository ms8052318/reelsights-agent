import { Button } from 'antd';
import React from 'react';

export default function VideoInstructionButton({ onHandleShow }) {
  return (
    <Button
      className="flex items-center rounded bg-[#1e263d] font-bold text-white"
      onClick={() => {
        onHandleShow();
      }}>
      Watch demo here
    </Button>
  );
}
