import { ClockCircleFilled } from '@ant-design/icons';
import { Button } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

export default function HistoryButton({ className }) {
  return (
    <Link to="/history" target="_blank" rel="noreferrer">
      <Button
        className="flex items-center rounded bg-[#1e263d] font-bold text-white"
        icon={<ClockCircleFilled />}>
        History
      </Button>
    </Link>
  );
}
