export const getPrenavigateDataFromSession = (key) => {
  let preNavigateData = null;
  try {
    preNavigateData = JSON.parse(window.sessionStorage.getItem(key)) || null;
    if (
      preNavigateData &&
      Object.keys(preNavigateData).length > 0 &&
      preNavigateData.href === window.location.href
    ) {
      return preNavigateData.data;
    } else {
      return null;
    }
  } catch (e) {
    console.log('getPrenavigateDataFromSession -> e', e);
    return null;
  }
};
