import { Button, Select } from "antd";
import { useState, useEffect } from "react";

export default function CustomSelect({
  form,
  name,
  options = [],
  className,
  multiple = false,
  exclusiveOption = [],
}) {
  useEffect(() => {
    const defaultValue = form.getFieldsValue()[name];
    if (defaultValue) setValue(defaultValue);
  }, [form.getFieldsValue()[name]]);

  const [value, setValue] = useState(multiple === true ? [] : "");
  const [disabled, setDisabled] = useState([]);

  const handleSelectOption = (newValue) => {
    if (multiple === false) {
      setValue(newValue);

      form.setFieldsValue({ [name]: newValue });
    } else {
      let updateValue = [];
      if (value.includes(newValue)) {
        updateValue = value.filter((item) => item !== newValue);
      } else {
        updateValue = [...value, newValue];
      }
      setValue(updateValue);
      form.setFieldsValue({ [name]: updateValue });
    }
  };

  useEffect(() => {
    if (multiple === false) {
      if (exclusiveOption.includes(value)) {
        setDisabled(
          options
            .filter((item) => exclusiveOption.includes(item.value))
            .map((item) => item.value)
        );
      } else {
        setDisabled([]);
      }
    } else {
      if (value.some((item) => exclusiveOption.includes(item))) {
        setDisabled(
          options
            .filter((item) => !exclusiveOption.includes(item.value))
            .map((item) => item.value)
        );
        setValue((prev) => {
          if (
            prev.filter((item) => !exclusiveOption.includes(item)).length > 0
          ) {
            form.setFieldsValue({
              [name]: prev.filter((item) => exclusiveOption.includes(item)),
            });
            return prev.filter((item) => exclusiveOption.includes(item));
          } else {
            return prev;
          }
        });
      } else {
        setDisabled([]);
      }
    }
  }, [value]);

  return (
    <div className="flex flex-wrap">
      {options.map((item, index) => (
        <Button
          disabled={disabled.includes(item.value)}
          size="large"
          icon={item.icon ? item.icon : null}
          className={`${
            (
              multiple === true
                ? value.includes(item.value)
                : value === item.value
            )
              ? "bg-linear-gradient-1 rounded-[20px] border-gray-300 text-white"
              : "rounded-[20px] border-gray-300 bg-white text-black"
          } ${className} mr-2 mb-2 flex items-center`}
          key={index}
          onClick={() => handleSelectOption(item.value)}
        >
          {item.label}
        </Button>
      ))}
    </div>
  );
}
