export const dataRoutesReelSy = [
  "/reelsy/content-creation/reelsy-ai-mini-campaign",
  "/reelsy/content-creation/reelsy-ai-audience",
];

const platform = process.env.REACT_APP_RS_ENDPOINT;

export const pageData = {
  name:
    platform === "https://api.reelsightsai.com"
      ? "ReelSights AI"
      : "Reelsights",
  logo: "/images/reelsights-logo.webp",
};
