import React, { useState, useEffect } from "react";
import { Button, Form, Input, Modal, Row, Select, notification } from "antd";
import {
  CheckCircleFilled,
  CloseOutlined,
  DownCircleOutlined,
  LinkedinOutlined,
  MinusCircleFilled,
  PlusCircleFilled,
  RedoOutlined,
  SendOutlined,
} from "@ant-design/icons";
import ToolQueryForm, {
  SubmitButton,
} from "../../components/Common/ToolQueryForm";
import {
  createParamsTranslationAWS,
  deleteCookie,
  formatDateToYYYYMMDDHHMM,
  getCookie,
  isLoggedInLinkedin,
  scrollToToolResultContainer,
  sendMessageErrorCommon,
} from "../../utils";
import CreationResult from "../../components/Common/CreationResult";
import { apiAiGenerateData } from "../../api/apiAiGenerateData";
import {
  getTextBetweenMarkers,
  handleAndGetDataAWSTranslation,
  handleAndGetDataCreation,
  handleContentReelsy,
} from "../../utils/contentCreation";

import InstructionContentWrapper from "../../components/Common/InstructionContentWrapper";
import { getPrenavigateDataFromSession } from "../../utils/navigateUtils";
import axios from "axios";
import { exportAsImageAndDownload } from "../../utils/html2image";

import CreateAndDownloadResultDoc from "../../components/Button/CreateAndDownloadResultDoc";
import { apiToolHistory } from "../../api/apiToolHistory";
import { useQuery } from "../../hooks/useQuery";
import languageList from "../../data/languageListAWS";
import ReelsyContentCreationLayout from "../../layouts/ReelsyContentCreationLayout";
import ToolResultRSAIContainer from "../../components/Common/ToolResultRSAIContainer";

const { TextArea } = Input;

const placeHolder = [
  "Ex: https://twitter.com/Nike",
  "Ex: https://www.linkedin.com/company/nike",
  "Ex: https://www.facebook.com/nike",
  "Ex: https://www.instagram.com/nike/",
];

const InstructionContent = () => (
  <InstructionContentWrapper>
    <div className="max-h-[85vh]">
      <div className="mb-4">
        <h1 className="mb-2 text-2xl text-black">Strategy Analysis</h1>
      </div>
      <div
        style={{
          margin: "0 auto",
        }}
      >
        <div className="relative w-full overflow-hidden pt-[56.25%]">
          <iframe
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              width: "100%",
              height: "100%",
              border: "none",
            }}
            src="https://www.youtube.com/embed/rRkHufSU__U?si=1WrDy0y0WUdvlocd"
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerPolicy="strict-origin-when-cross-origin"
            allowfullscreen
          ></iframe>
        </div>
      </div>
    </div>
  </InstructionContentWrapper>
);

export default function ReelSyAiStrat() {
  const query = useQuery();
  const hid = query.get("hid");
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [loadingGenerateLinkedinPost, setLoadingGenerateLinkedinPost] =
    useState(false);
  const [percent, setPercent] = useState(0);
  const [content, setContent] = useState(
    getPrenavigateDataFromSession("session_preNavigateData")?.content
  );
  const [username] = useState(getCookie("username"));
  const [fixedPrompt] = useState(
    `Hi ReelSy, as a Marketing Expert, you are also a marketing analyst, you specialize in marketing trends, analysis, and interpretation. Your responsibilities are: to help me analyze my marketing strategy's performance and extract meaningful insights that can guide my decision-making. I also want you to evaluate different aspects of my marketing strategy, including campaign goals, messaging, target audience, and channels. The goal is to identify my marketing’s strengths and weaknesses. ReelSy has deep Industry Knowledge: you have an understanding the industry landscape, market trends, and consumer behavior helps analysts put data in context and draw more relevant conclusions. I also want you to connect the dots in your analysis to form the bigger picture, you’ll understand the strategic goals of the marketing strategy and assess its alignment with the overall business objectives. 

    ReelSy’s knowledge includes the following: 
    You are a seasoned marketing strategist with 50 years of experience brings a wealth of knowledge and insights to the table when analyzing a brand’s marketing strategy. Let’s delve into what makes them effective:
    Deep Industry Understanding: Over five decades, you’ve witnessed market shifts, trends, and industry dynamics. You have extensive exposure allows you to anticipate changes, understand consumer behavior, and adapt strategies accordingly.
    Holistic Perspective: As seasoned strategist doesn’t view marketing in isolation. You  consider the entire business ecosystem, including product development, pricing, distribution channels, and customer touchpoints. This holistic approach ensures alignment with overall business goals.
    Market Research Mastery: With years of practice, You’’ve honed your skills in market research. You can analyze data, identify patterns, and extract meaningful insights. Your ability to interpret market trends informs strategic decisions.
    Brand Positioning Expertise: As a seasoned strategist you know how to position a brand effectively. You understand the nuances of brand identity, personality, and perception. Crafting a compelling brand story becomes second nature to you.
    Audience Insights: You’ve interacted with diverse audiences over the years. Their intuition and experience help them decode consumer preferences, pain points, and aspirations. This knowledge guides targeted messaging.
    Adaptability: Having navigated various marketing landscapes, you’re adept at adapting to new technologies, platforms, and channels. Your agility ensures that strategies remain relevant in an ever-evolving digital world.
    Data-Driven Decision-Making: You don’t rely solely on intuition. Instead, you use data-driven analysis to validate hypotheses, measure campaign effectiveness, and optimize marketing efforts.
    
    In summary, you are a blend of wisdom, adaptability, and strategic acumen makes them invaluable in dissecting and enhancing a brand’s marketing strategy. 
    
    ReelSy’s personality is inspired by these: 
    http://drewconway.com/; https://www.tomdavenport.com/; https://neilpatel.com/; https://twitter.com/randfish?lang=en; https://moz.com/community/q/user/randfish; https://serpstat.com/blog/; https://www.salesforce.com/blog/?d=70130000000i80c; https://blog.hootsuite.com/; https://sproutsocial.com/insights/; https://neilpatel.com/blog/ https://www.brandwatch.com/blog/; https://www.brandwatch.com/guides/; https://backlinko.com/blog 
    
    
    The goal is to provide detailed information on the pros and cons of my marketing strategy. I KNOW THAT YOU ARE MARKETING EXPERT. JUST GIVE ME THE ANSWER.
    
    Hi ReelSy, can you tell me the detailed marketing strategy analysis of these:     
     `
  );
  const [extendedAnalysisContent, setExtendedAnalysisContent] = useState("");
  const [identifyGapContent, setIdentifyGapContent] = useState("");
  const [loadingIdentifyGap, setLoadingIdentifyGap] = useState(false);
  const [
    generated10SeoOptimizedHeadlines,
    setGenerated10SeoOptimizedHeadlines,
  ] = useState("");
  const [
    loadingGenerate10SeoOptimizedHeadlines,
    setLoadingGenerate10SeoOptimizedHeadlines,
  ] = useState(false);
  const [currentQuery, setCurrentQuery] = useState(
    getPrenavigateDataFromSession("session_preNavigateData")?.content
  );
  const [dataChat, setDataChat] = useState([]);
  const [showLinkedinModal, setShowLinkedinModal] = useState(false);
  const [linkedinPostData, setLinkedinPostData] = useState(null);
  const [linkedinUserData, setLinkedinUserData] = useState({});
  const [loadingLinkedinUserData, setLoadingLinkedinUserData] = useState(false);
  const [isGeneratedLinkedinPost, setIsGeneratedLinkedinPost] = useState(false);
  const [linkedinPostHistory, setLinkedinPostHistory] = useState([]);
  const [showHistoryLinkedingPost, setShowHistoryLinkedingPost] =
    useState(false);
  const [loadingPostLinkedin, setLoadingPostLinkedin] = useState(false);
  const [showTwitterModal, setShowTwitterModal] = useState(false);
  const [historyId, setHistoryId] = useState(null);

  const [triggerCallCredit, setTriggerCallCredit] = useState({ count: 0 });
  const [apiNoti] = notification.useNotification();
  const [userSearchPreference, setUserSearchPreference] = useState({});
  const [userSearchPreferenceFields] = useState([
    "nameOfBusiness",
    "typeOfBusiness",
    "countryOrRegion",
    "companyUrl",
    "socialMediaUrl",
    "language",
  ]);

  useEffect(() => {
    if (
      showLinkedinModal &&
      isLoggedInLinkedin() === true &&
      !linkedinUserData.name &&
      isGeneratedLinkedinPost === true
    ) {
      setLoadingLinkedinUserData(true);
      axios
        .get(process.env.REACT_APP_VIDEO_URL + "/api/v1/linkedin/profile", {
          params: {
            access_token: getCookie("li_Tk"),
          },
        })
        .then((res) => {
          setLinkedinUserData(res.data);
        })
        .catch((err) => {
          sendMessageErrorCommon();
          deleteCookie("li_Tk");
        })
        .finally(() => {
          setLoadingLinkedinUserData(false);
        });
    }
  }, [showLinkedinModal, isGeneratedLinkedinPost]);

  useEffect(() => {
    const userLanguage =
      languageList[navigator.language || navigator.userLanguage] || "English";
    form.setFieldsValue({ language: userLanguage });
  }, []);

  const handleAddChat = async (dataChat, content) => {
    try {
      if (!content) return;
      const query = {
        msg: dataChat,
        query: content,
        email: getCookie("username"),
      };
      setDataChat((prev) =>
        prev && Array.isArray(prev) ? [...prev, { content, type: "user" }] : []
      );
      const newContent = (await apiToolHistory.getChatAssistant(query)).data;
      setDataChat(
        newContent.msg.map((item, index) =>
          item.role === "assistant"
            ? { ...item, content: handleContentReelsy(item.content) }
            : item
        )
      );
      return newContent;
      // scroll to bottom
    } catch {
      setDataChat((prev) =>
        prev && Array.isArray(prev) ? [...prev, { content, type: "user" }] : []
      );
    } finally {
      setTriggerCallCredit((preState) => ({ count: (preState.count += 1) }));
    }
  };

  const handleSubmit = async (
    data = {
      companyUrl: "",
      nameOfBusiness: "",
      typeOfBusiness: "",
      language: "",
    }
  ) => {
    try {
      setCurrentQuery(data);
      console.log("data", data);
      setLoading(true);
      const queryString = `${fixedPrompt}
      
      Hi ReelSy, can you tell me the marketing strategy of these: 
      Company Name: ${data.nameOfBusiness}
      Types of Business (in 50 words): ${data.typeOfBusiness}
      Company Link: ${data.companyUrl}
     `;
      if (
        process.env.REACT_APP_RS_ENDPOINT ===
        "https://beta-backend.reelsights.com"
      ) {
        console.log(queryString);
      }
      const newContent = await handleAddChat([], queryString);
      const handledContent = handleContentReelsy(
        handleAndGetDataCreation(newContent)
      );
      let translatedContent = "";
      if (data.language.includes("English") === false) {
        const queryParams = createParamsTranslationAWS({
          text: handledContent,
          language: data.language,
        });
        const dataCreateTwo = await apiAiGenerateData.translate(queryParams);
        translatedContent = handleAndGetDataAWSTranslation(dataCreateTwo);
      } else {
        translatedContent = handledContent;
      }
      setContent(translatedContent);
    } catch (e) {
      console.log(e);
      sendMessageErrorCommon();
    } finally {
      scrollToToolResultContainer();
      setLoading(false);
    }
  };

  const handleIndentifyGap = async () => {
    setLoadingIdentifyGap(true);
    try {
      const queryString = `Thanks, BASED ON YOUR RESPONSE ReelSy, please tell me what gaps and opportunities of this marketing strategy I can exploit in order to maximize my brand’s exposure and gain new customers. `;
      setDataChat(dataChat.slice(0, 4));
      const newContent = await handleAddChat(dataChat.slice(0, 4), queryString);
      const handledContent = handleContentReelsy(
        handleAndGetDataCreation(newContent)
      );
      let translatedContent = "";
      if (form.getFieldValue("language").includes("English") === false) {
        const queryParams = createParamsTranslationAWS({
          text: handledContent,
          language: form.getFieldValue("language"),
        });
        const dataCreateTwo = await apiAiGenerateData.translate(queryParams);
        translatedContent = handleAndGetDataAWSTranslation(dataCreateTwo);
      } else {
        translatedContent = handledContent;
      }

      setIdentifyGapContent(translatedContent);
    } catch {
      sendMessageErrorCommon();
    } finally {
      setLoadingIdentifyGap(false);
      setLoading(false);
    }
  };

  const handleGenerate10SeoOptimizedHeadlines = async () => {
    setLoadingGenerate10SeoOptimizedHeadlines(true);
    try {
      const queryString = `Thanks, BASED ON YOUR RESPONSE ReelSy, please create 10 SEO-optimised and emotional hook headlines that will exploit these gaps and opportunities for me to gain new leads. `;
      setDataChat(dataChat.slice(0, 4));
      const newContent = await handleAddChat(dataChat.slice(0, 4), queryString);
      const handledContent = handleContentReelsy(
        handleAndGetDataCreation(newContent)
      );
      let translatedContent = "";
      if (form.getFieldValue("language").includes("English") === false) {
        const queryParams = createParamsTranslationAWS({
          text: handledContent,
          language: form.getFieldValue("language"),
        });
        const dataCreateTwo = await apiAiGenerateData.translate(queryParams);
        translatedContent = handleAndGetDataAWSTranslation(dataCreateTwo);
      } else {
        translatedContent = handledContent;
      }
      setGenerated10SeoOptimizedHeadlines(translatedContent);
    } catch {
      sendMessageErrorCommon();
    } finally {
      setLoadingGenerate10SeoOptimizedHeadlines(false);
    }
  };

  const handleGenerateLinkedinPost = async () => {
    setLoadingGenerateLinkedinPost(true);
    try {
      const queryString = `Hi ReelSy, as a marketing expert, can you help me generate a Linkedin post based on your analysis, start with this sign &-----& and end with this sign &-----&`;
      setDataChat(dataChat.slice(0, 6));
      const newContent = await handleAddChat(dataChat.slice(0, 6), queryString);
      // remove text before and after -----

      setLinkedinPostData({
        text: getTextBetweenMarkers(
          handleContentReelsy(handleAndGetDataCreation(newContent)),
          "&-----&"
        ),
      });
      setIsGeneratedLinkedinPost(true);
    } catch {
      sendMessageErrorCommon();
    } finally {
      setLoadingGenerateLinkedinPost(false);
    }
  };

  const handleSubmitLinkedinPost = async () => {
    setLoadingPostLinkedin(true);
    try {
      axios
        .post(
          process.env.REACT_APP_VIDEO_URL + "/api/v1/linkedin/ugcPosts",
          {
            author: linkedinUserData.sub,
            text: linkedinPostData.text,
          },
          {
            params: {
              access_token: getCookie("li_Tk"),
            },
          }
        )
        .then((res) => {
          setShowHistoryLinkedingPost(true);
          setLinkedinPostData(null);
          setLinkedinPostHistory((prev) => [...prev, res.data]);
        })
        .catch((err) => {
          sendMessageErrorCommon();
        })
        .finally(() => {
          setLoadingPostLinkedin(false);
        });
    } catch {
      sendMessageErrorCommon();
    }
  };

  return (
    <ReelsyContentCreationLayout>
      <ToolQueryForm
        creditKey={"rs_ai_content_total"}
        triggerCallCredit={triggerCallCredit}
        title="Strategy Analysis"
        InstructionContent={InstructionContent}
        videoInstruction={true}
        listAbout={[
          {
            title: "Purpose",
            description:
              "Analyze your marketing strategies to find strengths, weaknesses, and areas for growth, and gain insights from your competitors.",
          },
          {
            title: "Benefits",
            description:
              "Identify gaps and opportunities in your strategies, and leverage insights from competitors.",
          },
          {
            title: "Next Steps",
            description:
              "Turn your headlines into content using <a href='/reelsy/content-creation/reelsy-ai-mini-campaign' target='_blank' rel='noopener noreferrer'>Mini Campaign</a> or <a href='/reelsy/content-creation/reelsy-ai-title-to-post' target='_blank' rel='noopener noreferrer'>Title To Post</a> (they use different training and data).",
          },
          {
            title: "Pro-Tip",
            description:
              "To achieve optimal results, include as many URL links as possible.",
          },
        ]}
        description={
          "Marketing strategists analyze your current strategy, pinpoint gaps, and identify exploitable opportunities for growth."
        }
      >
        <Form
          requiredMark={false}
          layout="vertical"
          name="control-hooks"
          id="tour-step-5"
          className="border-b-4 pb-4"
          onFinish={handleSubmit}
          form={form}
        >
          <div className="mb-4 gap-y-4 lg:grid lg:grid-cols-1 lg:gap-x-4">
            <div className="gap-x-4 lg:grid lg:grid-cols-2">
              <div>
                <Form.Item
                  name={"nameOfBusiness"}
                  label="Company Name"
                  rules={[
                    { required: true, message: "Please input company name!" },
                  ]}
                >
                  <Input size="large" placeholder="Ex: Nike" />
                </Form.Item>
                <Form.Item
                  name={"companyUrl"}
                  label="Company URL"
                  rules={[
                    {
                      required: true,
                      message: "Please input your company url!",
                    },
                  ]}
                >
                  <Input size="large" placeholder="www.nike.com" />
                </Form.Item>
              </div>
              <Form.Item
                name={"typeOfBusiness"}
                label="Describe the business in 50 words or less"
                rules={[
                  {
                    required: true,
                    message: "Please input your describe the business!",
                  },
                  // {
                  //   validator: async (_, value) => {
                  //     if (value && value.trim().split(' ').length > 10) {
                  //       return Promise.reject('Please input your business type in 20 words!');
                  //     }
                  //     return Promise.resolve();
                  //   },
                  // },
                ]}
              >
                <Input.TextArea
                  size="large"
                  style={{
                    resize: "none",
                    height: 135,
                  }}
                  placeholder="Explain what your business does or sells. For example, athletic footwear and apparel corporation"
                />
              </Form.Item>
            </div>

            <Form.List
              name="socialMediaUrl"
              initialValue={[
                {
                  socialMediaUrl: "",
                },
                {
                  socialMediaUrl: "",
                },
                {
                  socialMediaUrl: "",
                },
                {
                  socialMediaUrl: "",
                },
              ]}
            >
              {(fields, { add, remove }) => (
                <>
                  <div className="gap-x-4 lg:grid lg:grid-cols-2">
                    {fields.map((field, index) => {
                      return (
                        <Row
                          className="flex w-full items-center"
                          key={field.key}
                        >
                          <Form.Item
                            className="flex-1"
                            // key={field.key}
                            label={`Your Social Media URL ${index + 1}`}
                            shouldUpdate={true}
                            name={[field.name, "socialMediaUrl"]}
                          >
                            <Input
                              size="large"
                              maxLength={200}
                              name={`socialMediaUrl[${index}]`}
                              placeholder={
                                placeHolder[index] ||
                                "Put web or social media URL here... https://www.facebook.com/reelsights"
                              }
                            />
                          </Form.Item>
                          <PlusCircleFilled
                            onClick={() => {
                              if (fields.length < 5) {
                                add();
                              }
                            }}
                            className="mx-2 text-[20px] hover:cursor-pointer hover:opacity-80"
                          />
                          <MinusCircleFilled
                            onClick={() => {
                              if (fields.length > 2) {
                                remove(field.name);
                              }
                            }}
                            className="mx-2 text-[20px] hover:cursor-pointer hover:opacity-80"
                          />
                        </Row>
                      );
                    })}
                  </div>
                </>
              )}
            </Form.List>
            <div className="gap-x-4 lg:grid lg:grid-cols-4 ">
              <Form.Item
                name={"language"}
                className="col-span-1 col-end-5"
                label="Select language"
              >
                <Select
                  size="large"
                  placeholder="Language"
                  defaultValue={"English"}
                  disabled={loading || loadingIdentifyGap}
                  showSearch
                  options={Array.from(new Set(Object.values(languageList))).map(
                    (item) => ({
                      label: item,
                      value: item,
                    })
                  )}
                ></Select>
              </Form.Item>
            </div>
            <Form.Item>
              <SubmitButton loading={loading} text="Generate" />
            </Form.Item>
          </div>
        </Form>
      </ToolQueryForm>

      <div className="mt-4 md:px-[100px]">
        <ToolResultRSAIContainer isShow={!!content}>
          <CreationResult
            title="Strategy Analysis"
            content={content}
            handleContent={(value) => setContent(value)}
          />

          {currentQuery && (
            <Button
              className="mx-auto mt-2 flex h-auto items-center justify-center whitespace-normal rounded-[10px] border-0 bg-gray-100 font-medium text-primary hover:opacity-80 md:h-[40px]"
              onClick={() => handleSubmit(currentQuery)}
              icon={<RedoOutlined />}
              loading={loading}
            >
              Not a fan of this response, let’s generate another version
            </Button>
          )}
          <div className="my-2 space-y-2 md:flex md:space-y-0 md:space-x-2">
            {/* <Button
              onClick={() => setShowLinkedinModal(true)}
              className="bg-[#0A66C2] text-white flex items-center"
              icon={<LinkedinOutlined />}>
              Post to Linkedin
            </Button> */}

            <Button loading={loadingIdentifyGap} onClick={handleIndentifyGap}>
              Identify Gap and Opportunity{" "}
            </Button>
            <Button
              loading={loadingGenerate10SeoOptimizedHeadlines}
              onClick={handleGenerate10SeoOptimizedHeadlines}
            >
              Generate 10 SEO-Optimized Headlines
            </Button>
            <Button
              className="flex items-center bg-primary text-white"
              icon={<DownCircleOutlined />}
              onClick={async () => {
                await exportAsImageAndDownload(
                  document.getElementById("creation-result"),
                  "reelsy-ai-strat-creation-result-" +
                    formatDateToYYYYMMDDHHMM(new Date())
                );
              }}
            >
              Download the screenshot
            </Button>
            <CreateAndDownloadResultDoc
              content={content}
              title="Strategy Analysis"
            />
          </div>
          <div>
            <div className="mb-2">
              {extendedAnalysisContent && (
                <CreationResult
                  title="Extended Analysis"
                  content={extendedAnalysisContent}
                  handleContent={(extendedAnalysisContent) =>
                    setExtendedAnalysisContent(extendedAnalysisContent)
                  }
                />
              )}
            </div>
            <div>
              {identifyGapContent && (
                <CreationResult
                  title="Identify Gap and Opportunity"
                  content={identifyGapContent}
                  handleContent={(identifyGapContent) =>
                    setIdentifyGapContent(identifyGapContent)
                  }
                />
              )}
            </div>
            <div className="mt-2">
              {generated10SeoOptimizedHeadlines && (
                <CreationResult
                  title="Generated 10 SEO-Optimized Headlines"
                  content={generated10SeoOptimizedHeadlines}
                  handleContent={(generated10SeoOptimizedHeadlines) =>
                    setGenerated10SeoOptimizedHeadlines(
                      generated10SeoOptimizedHeadlines
                    )
                  }
                />
              )}
            </div>
          </div>
        </ToolResultRSAIContainer>
      </div>
      <Modal
        visible={showLinkedinModal}
        onCancel={() => setShowLinkedinModal(false)}
        footer={null}
        className="w-[80vw] p-5"
      >
        <Button
          className="mb-2 flex items-center rounded-[10px] bg-[#0A66C2] text-lg text-white"
          size="large"
          icon={<LinkedinOutlined />}
          onClick={handleGenerateLinkedinPost}
          loading={loadingGenerateLinkedinPost}
        >
          {isGeneratedLinkedinPost === false
            ? "Generate Linkedin Post"
            : "Regenerate Linkedin Post"}
        </Button>
        {isGeneratedLinkedinPost && (
          <div>
            {loadingLinkedinUserData === true && (
              <div className="mb-10 flex items-center space-x-4">
                <div className="h-[70px] w-[70px] animate-pulse bg-gray-200"></div>
                <div>
                  <h1 className="mb-0 h-[30px] animate-pulse bg-gray-200 text-xl font-semibold">
                    <div className="opacity-0">------------------</div>
                  </h1>
                  <h2 className="h-[20px] animate-pulse bg-gray-200">
                    <div className="opacity-0">------------------</div>
                  </h2>
                </div>
              </div>
            )}
            {loadingLinkedinUserData === false && (
              <div className="mb-10 flex items-center space-x-4">
                <img
                  src={
                    linkedinUserData.picture ||
                    "/images/linked-default-profile-picture.webp"
                  }
                  className="w-[70px] rounded-full"
                />
                <div>
                  <h1 className="mb-0 text-xl font-semibold">
                    {linkedinUserData.name}
                  </h1>
                  <h2>Post to anyone</h2>
                </div>
              </div>
            )}
            {linkedinPostData && (
              <Form onFinish={handleSubmitLinkedinPost}>
                <Form.Item>
                  <TextArea
                    className="custom-scroll-bar overflow-auto border-0 outline-none focus:shadow-none"
                    rows={14}
                    value={linkedinPostData.text}
                    onChange={(e) =>
                      setLinkedinPostData((prev) => ({
                        ...prev,
                        text: e.target.value,
                      }))
                    }
                    placeholder="What do you want to talk about?"
                  />
                </Form.Item>
                <Form.Item>
                  <Button
                    htmlType="submit"
                    loading={loadingPostLinkedin}
                    disabled={
                      loadingLinkedinUserData || loadingGenerateLinkedinPost
                    }
                    className="ml-auto flex items-center rounded-[2rem] bg-[#0A66C2] text-white"
                  >
                    Post <SendOutlined />
                  </Button>
                </Form.Item>
              </Form>
            )}
          </div>
        )}

        {showHistoryLinkedingPost && linkedinPostHistory.length > 0 && (
          <div className="relative rounded-[20px] border p-5 text-[25px]">
            <CloseOutlined
              className="absolute top-5 right-5 text-[16px] hover:cursor-pointer"
              onClick={() => setShowHistoryLinkedingPost(false)}
            />
            <h1 className="flex items-center ">
              <CheckCircleFilled className="mr-2 text-green-500" />
              Successfully posted to Linkedin,
              <a
                href={`https://www.linkedin.com/feed/update/${
                  linkedinPostHistory[linkedinPostHistory.length - 1].id
                }`}
                target="_blank"
                rel="noreferrer"
                className="ml-1"
              >
                {" "}
                click here to view
              </a>
            </h1>
          </div>
        )}
      </Modal>
    </ReelsyContentCreationLayout>
  );
}
