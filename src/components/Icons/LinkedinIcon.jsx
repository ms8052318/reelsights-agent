import React from 'react';

export default function LinkedinIcon({ className }) {
  return <img className={className} src={'/images/linkedin-logo.webp'} alt="create-my-video" />;
}
