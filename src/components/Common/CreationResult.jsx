import {
  TranslationOutlined,
  BulbOutlined,
  CopyOutlined,
} from "@ant-design/icons";
import { useState, useEffect } from "react";
import { Typography, Select, Tabs, Spin } from "antd";
import { getCookie } from "../../utils";

const { Paragraph, Title } = Typography;

export default function CreationResult({
  title,
  content = "",
  queryState,
  handleContent = () => {},
  mode = null,
}) {
  const [loadingCheck, setLoadingCheck] = useState(false);
  const [contentPlagiarism, setContentPlagiarism] = useState(null);
  const [language, setLanguage] = useState("English");
  const [loadingTranslate, setLoadingTranslate] = useState(false);
  const [contentTrans, setContentTrans] = useState("");
  const [username] = useState(getCookie("username"));

  console.log("queryState", queryState);

  useEffect(() => {
    setContentTrans("");
  }, [content]);

  return (
    <div id="creation-result" className="md:flex md:space-x-6">
      <div className="w-full bg-white ">
        <div>
          {title && (
            <Title level={4} className="flex items-center text-yellow-500">
              <BulbOutlined className="mr-2" />
              <span dangerouslySetInnerHTML={{ __html: title }}></span>
            </Title>
          )}
          <div className="flex items-center justify-start space-x-6 pb-2">
            {/* {hasCheckPlagiarism && (
              <button
                className="text-[#1890FF]"
                loading={loadingCheck}
                onClick={() => checkPlagiarism(content)}>
                Check Plagiarism
                {loadingCheck && <Spin className="ml-2" />}
              </button>
            )} */}
            {/* <button className="text-[#1890FF]" onClick={() => handleParaphrase()}>
              Paraphrase
            </button> */}

            {loadingTranslate && (
              <div className="ml-5 text-gray-400">
                <Spin className="mr-2" /> Translating
              </div>
            )}
          </div>
          {content && (
            <Paragraph
              copyable
              editable={{
                onChange: handleContent,
              }}
              className="whitespace-pre-line"
              id="content-result"
            >
              {content}
            </Paragraph>
          )}
        </div>
      </div>
      {(contentTrans || contentPlagiarism?.result_plagiarism) && (
        <div className="w-full bg-white p-6">
          {contentTrans && (
            <>
              <Title level={4} className="flex items-center text-green-500">
                <TranslationOutlined className="mr-2" />
                Translation ({language})
              </Title>
              {loadingTranslate ? (
                <Spin />
              ) : (
                <Paragraph
                  copyable
                  editable={{
                    onChange: handleContent,
                  }}
                  className="whitespace-pre-line"
                >
                  {contentTrans}
                </Paragraph>
              )}
            </>
          )}
          {contentPlagiarism?.result_plagiarism &&
          Array.isArray(contentPlagiarism?.result_plagiarism) &&
          contentPlagiarism?.result_plagiarism.length > 0 ? (
            <>
              <Title level={4} className="flex items-center text-red-500">
                <CopyOutlined className="mr-2" />
                Plagiarism
              </Title>
              {loadingCheck === false ? (
                contentPlagiarism?.result_plagiarism.map((item, index) => (
                  <Paragraph key={index}>
                    {index + 1}.{" "}
                    <a
                      href={contentPlagiarism?.result_link[index]}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {item}
                    </a>
                  </Paragraph>
                ))
              ) : (
                <Spin />
              )}
            </>
          ) : (
            contentPlagiarism?.result_plagiarism !== undefined && (
              <>
                <Title level={4}>Plagiarism</Title>
                <p>No plagiarism</p>
              </>
            )
          )}
        </div>
      )}
    </div>
  );
}
