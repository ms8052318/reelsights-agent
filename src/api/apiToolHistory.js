import axios from "axios";
import { getCookie } from "../utils";

// const historyServerUrl = process.env.REACT_APP_VIDEO_URL;
const historyServerUrl = "http://127.0.0.1:8080";

const axiosHistory = axios.create({
  baseURL: historyServerUrl,
  headers: {
    "Content-Type": "application/json",
  },
});

export const apiToolHistory = {
  getImageAI: ({ text }) =>
    axiosHistory.post(
      `/api/v1/image-generation-ai`,
      { text },
      { headers: { username: getCookie("username") } }
    ),
  getChatAssistant: ({ msg = [], query }) =>
    axiosHistory.post(
      `/api/v1/gemini/chat`,
      { msg, query },
      { headers: { username: getCookie("username") } }
    ),
};
