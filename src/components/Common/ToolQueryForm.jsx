import { SendOutlined } from "@ant-design/icons";
import { Button } from "antd";

export default function ToolQueryForm({
  children,
  title = "",
  description = "",
  SelectSection = () => null,
  summaryDescription = "YOUR BUSINESS OVERVIEW",
}) {
  return (
    <div className=" relative  max-w-full bg-[#d7eeed] pr-[50px] font-medium">
      <div>
        <div className="flex rounded-tr-[25px] bg-white pl-[100px] sm:block">
          <div className=" flex justify-between gap-2 bg-[#d7eeed]  text-[1.2rem] font-medium text-gray-200 max-sm:flex-wrap">
            <div className="flex w-full">
              <div className="flex w-[max-content] items-center rounded-tr-[25px] bg-white py-4 pr-4 font-bold text-[#1e263d]">
                {title}
                <div className="ml-5 h-[35px] w-[35px] rounded-[10px] bg-[#1e263d]"></div>
              </div>
            </div>
            <div className=" flex flex-col gap-2 xl:flex-row xl:items-center">
              <div className="flex items-center gap-2"></div>
            </div>
          </div>
        </div>
        <div className="relative rounded-tr-[25px] bg-white pb-5 pr-[50px] pl-[100px]">
          <div className="flex items-center justify-between py-5">
            <div className="">
              <div className="whitespace-pre-line text-[20px] font-bold">
                {summaryDescription}
              </div>
              <div className="whitespace-pre-line pr-5 text-sm text-[12px] text-gray-400">
                {description}
              </div>
            </div>
          </div>
          <div className="mb-5 w-full">
            <SelectSection />
          </div>
          <div style={{ display: "block" }}>{children}</div>
        </div>
      </div>
    </div>
  );
}

export const SubmitButton = ({
  loading,
  text = "Search",
  onClick = () => {},
  disabled = null,
}) => {
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      className="mx-auto flex h-[40px] min-w-[100px] items-center justify-center rounded-lg px-5 font-semibold text-white shadow-2xl md:min-w-[200px]"
      style={{
        background: disabled ? "#1e263db3" : "#1e263d",
      }}
      htmlType="submit"
      loading={loading}
    >
      {text} <SendOutlined />
    </Button>
  );
};
