import { rest } from "./rest";

export const apiAiGenerateData = {
  translate: (query) =>
    rest.postMapping("RS", "/translate_aws", query, null, true),
};
